import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { PasswordValidators } from '../password-validators';

@Component({
    selector     : 'auth-sign-up',
    templateUrl  : './sign-up.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignUpComponent implements OnInit
{
    @ViewChild('signUpNgForm') signUpNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signUpForm: FormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    // eslint-disable-next-line @typescript-eslint/member-ordering
    constructor(
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        // Used customised validators using RegEx
        this.signUpForm = this._formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            username: ['', Validators.maxLength(15)],
            email: ['', [Validators.required, Validators.email]],
            password: [
                '',
                [Validators.required,
                Validators.minLength(8),
                PasswordValidators.patternValidator(new RegExp('(?=.*[0-9])'), {
                    requiresDigit: true,
                }),
                PasswordValidators.patternValidator(new RegExp('(?=.*[A-Z])'), {
                    requiresUppercase: true,
                }),
                PasswordValidators.patternValidator(new RegExp('(?=.*[a-z])'), {
                    requiresLowercase: true,
                }),
                PasswordValidators.patternValidator(new RegExp('(?=.*[$@^!%*?&])'), {
                    requiresSpecialChars: true,
                }),
            ]]
        });
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    // auto generates username from firstname and lastname, maxlength is 15
    updateUsername(): void{
        const firstName = this.signUpForm.get('firstName').value;
        // console.log(firstName);
        const lastName = this.signUpForm.get('lastName').value;
        let username = firstName+lastName;
        if(username.length >14) {
            username = username.slice(0, username.length - (username.length - 14));
        }
        this.signUpForm.patchValue({username: username+'$'});
    }
    /**
     * Sign up
     */
    signUp(): void
    {
        // Do nothing if the form is invalid
        if ( this.signUpForm.invalid )
        {
            return;
        }

        // Disable the form
        this.signUpForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign up
        this._authService.signUp(this.signUpForm.value)
            .subscribe(
                (response) => {
                    response.subscribe(
                        (data)=>{
                            console.log(JSON.stringify(data));
                            localStorage.setItem(data.email , JSON.stringify(data));
                        }
                    );

                    // Navigate to the confirmation required page
                    this._router.navigateByUrl('/sign-in');
                },
                (response) => {
                    console.log(response);

                    // Re-enable the form
                    this.signUpForm.enable();

                    // Reset the form
                    // this.signUpNgForm.resetForm();

                    // Set the alert
                    this.alert = {
                        type   : 'error',
                        message: response
                    };

                    // Show the alert
                    this.showAlert = true;
                }
            );
    }
}
