import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private _httpClient: HttpClient,
  ) {}

  getCountries(): Observable<any>{
      return this._httpClient.get('https://countriesnow.space/api/v0.1/countries/positions');
  }
}
