import { Avatar } from './image';
import { ProfileService } from './profile.service';
import { UserService } from './../../core/user/user.service';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { PasswordValidators } from '../auth/password-validators';
import { Subject, takeUntil } from 'rxjs';
import { User } from 'app/core/user/user.types';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

    @ViewChild('updateNgForm') updateNgForm: NgForm;
    public countries = [];
    selectedFile: Avatar;

    user: User;
    updateForm: FormGroup;
    showAlert: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    private avatarSrc: any;

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _userService: UserService,
    private _profileService: ProfileService
  ) { }

  ngOnInit(): void {

    this._profileService.getCountries()
    .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((countries: any) => {
            this.countries = countries.data;
            // console.log(this.countries[0].name);
        });
    this.user = this._userService.user$;
    this.updateForm = this._formBuilder.group({
                firstName: [this.user.firstName, Validators.required],
                lastName: [this.user.lastName, Validators.required],
                username: [this.user.username],
                email: [this.user.email],
                address: [this.user.address],
                phoneNo: [this.user.phoneNo],
                country: [this.user.country],
                avatar: ['']
            });

    // this._userService.user$
    // .pipe(takeUntil(this._unsubscribeAll))
    // .subscribe((user: User) => {
    //     this.user = user;
    //     console.log(user);
    //     this.updateForm = this._formBuilder.group({
    //         firstName: [user.firstName, Validators.required],
    //         lastName: [user.lastName, Validators.required],
    //         username: [user.username],
    //         email: [user.email],
    //         address: [user.address],
    //         phoneNo: [user.phoneNo]
    //     });
    // });
  }

  ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    changeCity(e: any): void {
        this.updateForm.get('country')?.setValue(e.target.value, {
            onlySelf: true,
          });
    }

    processFile(event): void {
        const file: File = event.target.files[0];
        const reader = new FileReader();

        if(file) {
            reader.readAsDataURL(file);
            // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
            reader.onload = () => {
                console.log(reader.result);
                this.avatarSrc = reader.result;
                // this.updateForm.get('avatar').setValue(reader.result);

            };
        }
        // this.updateForm.get('avatar').setValue(reader.result);
      }

    // gets the values from the form, appends password with form data
    // and the information is updated in local storage
  updateProfile(): void
    {
        // Do nothing if the form is invalid
        if ( this.updateForm.invalid )
        {
            return;
        }

        // Disable the form
        this.updateForm.disable();

        // Hide the alert
        this.showAlert = false;
        // console.log(this.updateForm.value);
        const password = this.user.password;

        const updatedUser = {...this.updateForm.value, password, id: this.user.id, avatar: this.avatarSrc};
        localStorage.setItem(this.user.email , JSON.stringify(updatedUser));
        this._router.navigateByUrl('/posts');
        console.log(updatedUser);
    }

}
