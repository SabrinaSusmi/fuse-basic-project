import { PostService } from './../../post.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FuseAlertType } from '@fuse/components/alert';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
})
export class CreatePostComponent implements OnInit, OnDestroy {
    @ViewChild('createPostNgForm') createPostNgForm: NgForm;
    user: User;
    createPostForm: FormGroup;
    showAlert: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    // eslint-disable-next-line @typescript-eslint/member-ordering
    im = JSON.parse(localStorage.getItem('demoprof'));

    constructor(
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _userService: UserService,
        private _postService: PostService
    ) { }




    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    ngOnInit(): void {
        this.user = this._userService.user$;
        console.log(this.user);

    this.createPostForm = this._formBuilder.group({
        title: ['', Validators.required],
        body: ['', Validators.required],
        privacy: ['', Validators.required]
    });
    }

    // post is saved to local storage
    createPost(): void {
        // Do nothing if the form is invalid
        if ( this.createPostForm.invalid )
        {
            return;
        }

        // Disable the form
        this.createPostForm.disable();

        // Hide the alert
        this.showAlert = false;
        const posts = JSON.parse(localStorage.getItem('allposts'));
        const data = {...this.createPostForm.value, id: posts.length+1, userId: this.user.id, username: this.user.firstName+' '+ this.user.lastName};
        console.log(data);
        posts.unshift(data);
        // console.log(posts);

        localStorage.setItem('allposts', JSON.stringify(posts));
        this._router.navigateByUrl('/posts');

        // this._postService.createPost(this.createPostForm.value)
        // .subscribe(
        //     (response) => {
        //         response.subscribe(
        //             (data)=>{
        //                 const posts = JSON.parse(localStorage.getItem('allposts'));
        //                 data.id = posts.length;
        //                 data = {...data, userId: this.user.id,};
        //                 console.log(data);
        //                 posts.unshift(data);
        //                 console.log(posts);

        //                 localStorage.setItem('allposts', JSON.stringify(posts));
        //             }
        //         );

        //         // Navigate to the confirmation required page
        //         this._router.navigateByUrl('/posts');
        //     },
        //     (response) => {

        //         // Re-enable the form
        //         this.createPostForm.enable();

        //         // Reset the form
        //         this.createPostNgForm.resetForm();

        //         // Set the alert
        //         this.alert = {
        //             type   : 'error',
        //             message: 'Something went wrong, please try again.'
        //         };

        //         // Show the alert
        //         this.showAlert = true;
        //     }
        // );
    }

}
