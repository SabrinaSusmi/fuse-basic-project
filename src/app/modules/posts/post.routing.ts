import { PostEditComponent } from './post-edit/post-edit/post-edit.component';
import { PostDetailsComponent } from './post-details/post-details/post-details.component';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { CreatePostComponent } from './create-post/create-post/create-post.component';
import { Route } from '@angular/router';
import { PostListComponent } from './post-list/post-list.component';

export const postRoutes: Route[] = [
    {
        path     : '',
        component: PostListComponent,
    },
    {
        path     : 'create-post',
        component: CreatePostComponent,
        canActivate: [AuthGuard]
    },
    {
        path     : 'details/:id',
        component: PostDetailsComponent,
        children : [
            {path: 'edit/:id', component: PostEditComponent, canActivateChild: [AuthGuard]}
        ]
    }
];
