import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseAlertType } from '@fuse/components/alert';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import { Subject, takeUntil } from 'rxjs';
import { PostService } from '../../post.service';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
})
export class PostEditComponent implements OnInit, OnDestroy {
    @ViewChild('editPostNgForm') editPostNgForm: NgForm;
    user: User;
    editPostForm: FormGroup;
    showAlert: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    // eslint-disable-next-line @typescript-eslint/member-ordering
    public post: any;
    private postlist = JSON.parse(localStorage.getItem('allposts'));
    private postId: any;

    constructor(
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _userService: UserService,
        private _postService: PostService,
        private route: ActivatedRoute
    ) { }




    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    ngOnInit(): void {
        // this._userService.user$
        // .pipe(takeUntil(this._unsubscribeAll))
        // .subscribe((user: User) => {
        //     this.user = user;
        //     console.log(user);
        // });

        this.user = this._userService.user$;

    this.postId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    // console.log(this.route.snapshot.paramMap.get);
    this.post = this.postlist.find(el=> el.id === this.postId);
    console.log(this.post);

    this.editPostForm = this._formBuilder.group({
        title: [this.post.title, Validators.required],
        body: [this.post.body, Validators.required],
        privacy: [this.post.privacy, Validators.required]
    });
    }

    // edited post is reset in the local storage
    editPost(): void {
        // Do nothing if the form is invalid
        if ( this.editPostForm.invalid )
        {
            return;
        }

        // Disable the form
        this.editPostForm.disable();

        // Hide the alert
        this.showAlert = false;
        const indx = this.postlist.findIndex(el=> el.id === this.postId);

        const updatedPost = {...this.editPostForm.value, id: this.post.id, userId: this.user.id, username: this.user.firstName+' '+ this.user.lastName};
        this.postlist[indx] = updatedPost;
        console.log(this.postlist);
        localStorage.setItem('allposts', JSON.stringify(this.postlist));
        this._router.navigateByUrl(`/posts/details/${this.post.id}`);
    }

}
