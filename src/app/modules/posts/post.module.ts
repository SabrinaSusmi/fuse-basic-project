import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import { FuseCardModule } from '@fuse/components/card';
import { MatIconModule } from '@angular/material/icon';
import { postRoutes } from './post.routing';
import { PostListComponent } from './post-list/post-list.component';
import { CreatePostComponent } from './create-post/create-post/create-post.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FuseAlertModule } from '@fuse/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { PostDetailsComponent } from './post-details/post-details/post-details.component';
import { PostEditComponent } from './post-edit/post-edit/post-edit.component';
import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  declarations: [
    PostListComponent,
    CreatePostComponent,
    PostDetailsComponent,
    PostEditComponent
  ],
  imports: [
    RouterModule.forChild(postRoutes),
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    FuseCardModule,
    FuseAlertModule,
    SharedModule,
    MatRadioModule
  ]
})
export class PostModule { }
