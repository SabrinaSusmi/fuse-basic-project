import { UserService } from 'app/core/user/user.service';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, map, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Post } from './post.types';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private _httpClient: HttpClient,
  ) {}

  getPosts(): Observable<any>{
    // return this._httpClient.get<Post[]>('https://jsonplaceholder.typicode.com/posts');
      const data = JSON.parse(localStorage.getItem('allposts'));
      if(data) {
        return of(data);
      } else {
        return this._httpClient.get<Post[]>('https://jsonplaceholder.typicode.com/posts');
      }
  }
  createPost(post: { title: string; body: string }): Observable<any>
     {
        const res = this._httpClient.post('https://jsonplaceholder.typicode.com/posts', post);

        return of(res);
     }
}
