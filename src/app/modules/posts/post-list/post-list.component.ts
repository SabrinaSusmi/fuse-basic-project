import { AuthService } from './../../../core/auth/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Subject, takeUntil, map } from 'rxjs';
import { PostService } from '../post.service';
import { User } from 'app/core/user/user.types';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class PostListComponent implements OnInit, OnDestroy {
    user: User;
    public postList = [];
    private _unsubscribeAll: Subject<any> = new Subject<any>();

  constructor(
      private _postService: PostService,
      private _router: Router,
      private _authService: AuthService,
      private _userService: UserService,
  ) { }

  ngOnInit(): void {
    this.user = this._userService.user$;
      this._postService.getPosts()
      .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((postList: any) => {
                // localStorage.removeItem('allposts');

                this.postList = postList;
                if(!(localStorage.getItem('allposts'))) {
                    this.postList.forEach((post) => {
                        if((Math.floor((Math.random() * 2) + 1))===1) {
                            post.privacy = 'public';
                        } else {
                            post.privacy = 'private';
                        }
                    });
                    console.log('if entered');
                    localStorage.setItem('allposts', JSON.stringify(this.postList));
                } else {
                    if(this._authService.check()){
                        const list = [];
                        this.postList.forEach((post) => {
                            if(post.privacy==='public'||post.userId===this.user.id) {
                                list.push(post);
                            }
                        });
                        this.postList = list;
                        // this.postList = this.postList.find(post=> ((post.privacy===1)||(post.userId===this.user.id)));
                        console.log(this.postList);
                    } else {
                        const list = [];
                        console.log(this.postList);
                        this.postList.forEach((post) => {
                            if(post.privacy===1||post.privacy==='public') {
                                list.push(post);
                            }
                        });
                        this.postList = list;
                        console.log(this.postList);
                    }
                }
                console.log(this.postList);
            });
    }

  ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    createPost(): void {
        this._router.navigateByUrl('/posts/create-post');
    }

}
