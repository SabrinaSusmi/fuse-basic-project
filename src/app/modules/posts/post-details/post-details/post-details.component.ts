import { ModalComponent } from './../../../../core/modal/modal/modal.component';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from 'app/core/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'app/core/user/user.types';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
})
export class PostDetailsComponent implements OnInit, OnDestroy {
    public editAccess: boolean = false;
    user: User;
    public post: any;
    private postlist = JSON.parse(localStorage.getItem('allposts'));
    private _unsubscribeAll: Subject<any> = new Subject<any>();

  constructor(
      private route: ActivatedRoute,
      private _userService: UserService,
      private _router: Router,
      private dialogRef: MatDialog
  ) { }

  ngOnInit(): void {
      this.user = this._userService.user$;
      const postId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
      this.post = this.postlist.find(el=> el.id === postId);
      console.log(this.post.userId);

    if(this.user){
        if(this.post.userId===this.user.id){
            this.editAccess=true;
        }
    }
  }
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next(null);
      this._unsubscribeAll.complete();
  }

  deletePost(): void {
    console.log('entered delete');
    const indx = this.postlist.findIndex(el=> el.id === this.post.id);
    this.postlist.splice(indx, 1);
    localStorage.setItem('allposts',JSON.stringify(this.postlist));
    this._router.navigateByUrl('/posts');
  }

  openDialogue(): void {
      const dialog = this.dialogRef.open(ModalComponent,{
        width: '250px'
      });
      dialog.afterClosed().subscribe((result) => {
        console.log('The dialog was closed');
        if(result) {
            this.deletePost();
        }
      });
  }

}
