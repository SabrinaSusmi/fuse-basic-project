import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { UserService } from 'app/core/user/user.service';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService
{
    // eslint-disable-next-line max-len
    public accessToken: any = 'eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTY1NDY3NzkyNCwiaWF0IjoxNjU0Njc3OTI0fQ.eQaM9I_0PapUmz8ppD3CCWqilznltlNb5rdV0NcO-1c';

    private _authenticated: boolean = false;
    private response: any = null;
    // eslint-disable-next-line max-len

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService
    )
    {
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    /**
     * Sign in
     *
     * @param credentials
     * // returns response as an Observable of type <any>.
     * // the user activation flag is set to true and current user information is set in UserService
     */

    signIn(credentials: { email: string; password: string }): Observable<any>
    {
        // Throw error, if the user is already logged in
        if ( this._authenticated )
        {
            return throwError('User is already logged in.');
        }

        console.log('before parsing');
        if(localStorage.getItem(credentials.email)) {
            this.response = JSON.parse(localStorage.getItem(credentials.email));
            console.log(this.response);
            // console.log(bcrypt.compareSync(credentials.password, this.response.password));
            if(bcrypt.compareSync(credentials.password, this.response.password)){
                this._userService.user = this.response;
                this._userService.active = true;
                this._authenticated = true;
                // localStorage.setItem('userActive', JSON.stringify(true));
                return of(this.response);
            } else {
                return throwError('Password does not match!');
            }
        }
        else {
            return throwError('User not registered!');
        }
    }

    /**
     * Sign up
     *
     * @param user
     * // returns user data as an observable of type <any>
     */
     signUp(user: { firstName: string; lastName: string; email: string; password: string; company: string }): Observable<any>
     {
         if(localStorage.getItem(user.email)) {
             return throwError('User exists with this email address');
         }
        const salt = bcrypt.genSaltSync(10);
        user.password = bcrypt.hashSync(user.password, 10);

        const res = this._httpClient.post('https://jsonplaceholder.typicode.com/users', user);

         return of(res);
     }

    /**
     * Sign out
     */

    signOut(): Observable<any>
    {
        // set local storage authenticated flag to false
        this._userService.active = false;
        this._userService.user = null;

        // Set the authenticated flag to false
        this._authenticated = false;

        // Return the observable
        return of(true);
    }


    /**
     * Check the authentication status
     */
    check(): boolean
    {
        // Check if the user is logged in
        if ( JSON.parse(localStorage.getItem('userActive')) )
        {
            // console.log('auth service check function');
            return true;
        } else{
            return false;
        }
    }
}
