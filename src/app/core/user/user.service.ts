import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, map, Observable, of, ReplaySubject, tap } from 'rxjs';
import { User } from 'app/core/user/user.types';

@Injectable({
    providedIn: 'root'
})
export class UserService
{
    public _activatedUser: boolean;
    private _user = new BehaviorSubject(false);


    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
        this._user.next(JSON.parse(localStorage.getItem('userActive')));
    }


    /**
     * Setter & getter for user activation flag
     *
     * @param value
     */

    get userA(): Observable<boolean>{
        return this._user.asObservable();
    }

     get active$(): boolean
     {
        return JSON.parse(localStorage.getItem('userActive'));
     }

     set active(value: boolean)
    {
        this._user.next(value);
        localStorage.setItem('userActive',JSON.stringify(value));
    }

    /**
     * Setter & getter for user
     *
     * @param value
     */

     // eslint-disable-next-line @typescript-eslint/member-ordering
     get user$(): User
     {
        return JSON.parse(localStorage.getItem('currentUser'));
     }

    set user(value: User)
    {
        // Store the value
        localStorage.setItem('currentUser', JSON.stringify(value));
    }
}
