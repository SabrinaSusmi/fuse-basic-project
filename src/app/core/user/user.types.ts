export interface User
{
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    avatar?: string;
    status?: string;
    address?: string;
    phoneNo?: string;
    country?: string;
    password: string;
}
