import { Navigation } from './navigation.types';
import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class NavigationService
{
    private defaultNavigation: {id: string; title: string; type: string; icon: string; link: string }[] = [
        {
            id   : 'sign-in',
            title: 'Sign-In',
            type : 'basic',
            icon : 'heroicons_outline:chart-pie',
            link : '/sign-in'
        },
        {
            id   : 'posts',
            title: 'Posts',
            type : 'basic',
            icon : 'heroicons_outline:chart-pie',
            link : '/posts'
        }
    ];

    private afterAuthnav: {id: string; title: string; type: string; icon: string; link: string }[] = [
        {
            id   : 'home',
            title: 'Home',
            type : 'basic',
            icon : 'heroicons_outline:chart-pie',
            link : '/home'
        },
        {
            id   : 'posts',
            title: 'Posts',
            type : 'basic',
            icon : 'heroicons_outline:chart-pie',
            link : '/posts'
        }
    ];
    // private profileNav = this.defaultNavigation.splice(0,1);
    private nav: BehaviorSubject<any> = new BehaviorSubject<any>(0);

    /**
     * Constructor
     */
    constructor()
    {
        this.nav.next(this.defaultNavigation);

    }

    /**
     * Getter for navigation
     */
    get navigation$(): Observable<any>
    {
        return of(this.nav);
    }

    setProfileNav(): void{
        console.log('entered in prof nav');
        this.nav.next(this.afterAuthnav);
    }

    setDefaultNav(): void {
        this.nav.next(this.defaultNavigation);
    }

}
